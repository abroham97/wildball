﻿using UnityEngine;
using UnityEngine.UI;

namespace WildBall.Menu
{
    public class ShowingButtonMessage : MonoBehaviour
    {
        [SerializeField] private GameObject _panelInfo;

        public void ShowMessage(string button)
        {
            _panelInfo.SetActive(true);
        }

        public void HideMessage()
        {
            _panelInfo.SetActive(false);
        }
    }
}