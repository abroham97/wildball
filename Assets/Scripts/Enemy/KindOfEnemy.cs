﻿namespace WildBall.Enemies
{
    public enum KindOfEnemy
    {
        Lava = 0,
        Guard = 1,
        Ghost = 2
    }
}