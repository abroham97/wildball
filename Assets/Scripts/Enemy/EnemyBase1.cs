using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using WildBall.Inputs;

namespace WildBall.Enemies
{
    public abstract class EnemyBase : MonoBehaviour
    {
        [SerializeField] private KindOfEnemy _kindOfEnemy;
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(TagsVars.PLAYER))
            {
                var playerMovementController = other.GetComponent<PlayerMovementController>();
                var DeathScript = other.GetComponent<DeathScript>();
                playerMovementController.StopMovement();
                StartCoroutine(Hit(DeathScript));
            }
        }

        public virtual IEnumerator Hit(DeathScript DeathScript)
        {
            DeathScript.ShowDeath(_kindOfEnemy);
            yield return new WaitForSeconds(1f);
        }
    }
}