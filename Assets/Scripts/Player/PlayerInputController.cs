using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildBall.Inputs
{
    [RequireComponent(typeof(PlayerMovementController))]
    public class PlayerInputController : MonoBehaviour
    {
        private Vector3 _movement;
        private PlayerMovementController _playerMovementController;

        private void Awake()
        {
            _playerMovementController = GetComponent<PlayerMovementController>();
        }

        private void Update()
        {
            var horizontal = Input.GetAxis(InputVars.HORIZONTAL_AXIS);
            var vertical = Input.GetAxis(InputVars.VERTICAL_AXIS);

            _movement = new Vector3(-horizontal, 0, -vertical).normalized;
        }
        
        private void FixedUpdate()
        {
            _playerMovementController.MoveCharacter(_movement);
        }
    }
}