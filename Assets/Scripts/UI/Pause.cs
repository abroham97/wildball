using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    private float timer;
    private bool _gamePaused;
    public GameObject _canvasPause;

    void Update()
    {
        Time.timeScale = timer;
        if (Input.GetKeyDown(KeyCode.Escape) && _gamePaused == false)
        {
            _gamePaused = true;
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && _gamePaused == true)
        {
            _gamePaused = false;
        }
        if (_gamePaused == true)
        {
            timer = 0;
            _canvasPause.SetActive(true);

        }
        else if (_gamePaused == false)
        {
            timer = 1f;
            _canvasPause.SetActive(false); 

        }
    }


    public void Continue()
    {
        timer = 1f;
        _canvasPause.SetActive(false);
        _gamePaused = false;
    }
    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }
}
