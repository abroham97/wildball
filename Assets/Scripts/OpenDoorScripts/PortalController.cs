using UnityEngine;
using UnityEngine.SceneManagement;
using WildBall;

namespace WildBall.Environment
{
    public class PortalController : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(TagsVars.PLAYER))
            {
                var activeScene = SceneManager.GetActiveScene();
                var nextScene = activeScene.buildIndex + 1;
                if (nextScene >= SceneManager.sceneCountInBuildSettings)
                {
                    Debug.Log($"Отсутствует сцена под индексом {nextScene}");
                }
                else
                {
                    SceneManager.LoadScene(nextScene);
                }
            }
        }
    }
}