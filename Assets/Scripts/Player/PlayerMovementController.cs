using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildBall.Inputs
{
    public class PlayerMovementController : MonoBehaviour
    {
        [SerializeField, Range(0, 10)] private float _speed = 2.0f;
        private Rigidbody _playerRigidbody;
        private bool _isActive;
        
        private void Awake()
        {
            _isActive = true;
            _playerRigidbody = GetComponent<Rigidbody>();
        }

        public void MoveCharacter(Vector3 movement)
        {
            if (_isActive)
            {
                _playerRigidbody.AddForce(movement * _speed);   
            }
        }

        public void StopMovement()
        {
            _isActive = false;
            _playerRigidbody.constraints = RigidbodyConstraints.FreezePosition;
        }
    }
}