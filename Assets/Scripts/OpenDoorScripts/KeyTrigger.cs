using UnityEngine;
using WildBall;

namespace WildBall.Environment
{
    public class KeyTrigger : MonoBehaviour
    {
        [SerializeField] private GameObject _portal;
        [SerializeField] private GameObject _key;

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.CompareTag(TagsVars.PLAYER))
            {
                ActivatedPortal();
                DestroyKey();
            }
        }

        private void ActivatedPortal()
        {
            _portal.SetActive(true);
        }

        private void DestroyKey()
        {
            Destroy(_key);
        }
    }
}