using UnityEngine;
using WildBall.Enemies;

namespace WildBall.Inputs
{
    public class DeathScript : MonoBehaviour
    {
        [SerializeField] private ParticleSystem _particleSystemLava;
        [SerializeField] private ParticleSystem _particleSystemGuard;
        [SerializeField] private ParticleSystem _particleSystemGhost;
        [SerializeField] private GameObject _loseScreen;

        public void ShowDeath(KindOfEnemy kind)
        {
            switch (kind)
            {
                case KindOfEnemy.Lava:
                    ShowParticleSystem(_particleSystemLava);
                    break;
                case KindOfEnemy.Guard:
                    ShowParticleSystem(_particleSystemGuard);
                    break;
                case KindOfEnemy.Ghost:
                    ShowParticleSystem(_particleSystemGhost);
                    break;
                default:
                    ShowParticleSystem(null);
                    break;
            }

            gameObject.SetActive(false);
            _loseScreen.SetActive(true);
        }

        private void ShowParticleSystem(ParticleSystem ps)
        {
            ps.gameObject.transform.position = transform.position;
            ps.gameObject.SetActive(true);
        }
    }
}