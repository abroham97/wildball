using UnityEngine;
using WildBall.Menu;

namespace WildBall.Environment
{
    public class OpeningDoorTrigger : MonoBehaviour
    {
        private const string OPENING_TRIGGER = "Opening";

        [SerializeField] private ShowingButtonMessage _helpMessageInfo;

        private Animator _parentAnimator;
        private bool _isCanOpen;
        private bool _isOpen;

        private void Awake()
        {
            _parentAnimator = GetComponentInParent<Animator>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(TagsVars.PLAYER) && !_isOpen)
            {
                _helpMessageInfo.ShowMessage(nameof(KeyCode.E));
                _isCanOpen = true;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag(TagsVars.PLAYER))
            {
                HideMessage();
            }
        }

        private void FixedUpdate()
        {
            if (_isCanOpen && Input.GetKey(KeyCode.E))
            {
                _parentAnimator.SetTrigger(OPENING_TRIGGER);
                _isOpen = true;
                HideMessage();
            }
        }

        private void HideMessage()
        {
            _helpMessageInfo.HideMessage();
            _isCanOpen = false;
        }
    }
}