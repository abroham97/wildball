using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeScenesScript : MonoBehaviour
{
    [SerializeField] private GameObject _mainMenu;
    [SerializeField] private GameObject _levelsMenu;

    private void Start()
    {
    }

    public void loadlevel(int LevelIndex)
    {
        SceneManager.LoadScene(LevelIndex);
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene(0);
        _levelsMenu.SetActive(false);
        _mainMenu.SetActive(true);
    }

    public void ExitGame()
    {
        Application.Quit();
        Debug.Log("Exited!!!");
    }
    public void StartGame()
    {
        _mainMenu.SetActive(false);
        _levelsMenu.SetActive(true);
    }
}
